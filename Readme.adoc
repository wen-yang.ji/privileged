= Privileged

//tag::abstract[]

Docker starts containers with a restricted set of capabilities.  However,
privileged mode gives all capabilities to the container, and it also lifts all
the limitations enforced by the device cgroup controller. 
In other words, the container can then do almost everything that the host can
do. 

//end::abstract[]

By default, Docker containers are “unprivileged” and cannot, for example, run a
Docker daemon inside a Docker container. This is because by default a container
is not allowed to access any devices, but a “privileged” container is given
access to all devices.

When the operator executes docker run --privileged, Docker will enable access
to all devices on the host as well as set some configuration in AppArmor or
SELinux to allow the container nearly all the same access to the host as
processes running outside containers on the host.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

image::https://gitlab.com/insecure-programming/docker/privileged/badges/master/pipeline.svg[]

=== Task 0

*Fork* and clone this repository.
Install `docker`, `docker-compose` and `make` on your system.

. Run unit tests: `make test`. 
. Build the program: `make build`.
. Run it: `make run`.
. Run security tests: `make securitytest`.

Note: The last test will fail. 


=== Task 1

Review `Dockerfile` and `docker-compose.yml` file. 
Find out the security issue and why tests fails.

Note: Avoid looking at tests and try to
spot the vulnerability on your own.

=== Task 2

The security weakness is running docker in privileged mode.
Simply modify `docker-compose.yml` to disable privileged mode.

=== Task 3

The `privleged: true` should be set to false. 
Run security tests again. Make sure build pass.
If you stuck, move to the next task.

=== Task 4 
 
Check out the `patch` branch and review the program code. 
Run all tests and make sure everything pass. 
 
=== Task 5 
 
Merge the patch branch to master. 
Commit and push your changes. 
Does pipeline for your forked repository show that you have passed the build?   

(Note: you do NOT need to send a pull request)

//end:lab[]

//tag::references[]

== References

//end::references[]
